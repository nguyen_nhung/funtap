from datetime import datetime, timedelta
import subprocess
import glob
import os
import time
import pandas as pd
import shutil

PATH = './data/'
gz_path_event = PATH + "gz/event/"
gz_path_install = PATH + "gz/install/"
parquet_path = PATH + "parquet/"

API_INAPPS = "aws s3 sync s3://af-ext-reports/66d8-acc-3547cf45-66d8/data-locker-hourly/t=inapps/dt="
API_INSTALLS = "aws s3 sync s3://af-ext-reports/66d8-acc-3547cf45-66d8/data-locker-hourly/t=installs/dt="
HOUR_DAY = list(range(17,24)) + list(range(17))
HOUR_DAY.append(str("late"))
LIST_DATE = ["2021-06-05", "2021-06-06", "2021-06-07", "2021-06-08", "2021-06-09", "2021-06-10"] # list day crawl data (exmple LIST_DATE = ['2021-06-09', '2021-06-10', '2021-06-15'])

def aws_sync(date_time):
    install_command = API_INSTALLS
    command = API_INAPPS
    cmd_list = []
    for i in HOUR_DAY:
        if (type(i)!=str) and i<17:
            cmd_list.append(command + str(date_time) + "/h=" + str(i) + "/ " + gz_path_event + str(date_time)+ "/" + str(i))
            cmd_list.append(install_command + str(date_time) + "/h=" + str(i) + "/ " + gz_path_install + str(date_time) + "/" + str(i))
        else:
            first_day = date_time - timedelta(1)
            cmd_list.append(command + str(first_day) + "/h=" + str(i) + "/ " + gz_path_event + str(date_time) + "/" + str(i))
            cmd_list.append(install_command + str(first_day) + "/h=" + str(i) + "/ " + gz_path_install + str(date_time) + "/" + str(i))
    return cmd_list

def unzip_csv(date_time):
    s_file = "_SUCCESS"
    gz_folders = glob.glob(gz_path_event + date_time + "/*/")
    downloaded_list = []
    list_cmd = []
    for folders in gz_folders:
        if os.path.exists(folders + s_file):
            downloaded_list.append(folders)
    if len(downloaded_list) != len(HOUR_DAY):
        for i in range(len(downloaded_list)):
            cmd = "find " + downloaded_list[i] + " -name \"*.gz\" " \
                                                 "| xargs gunzip -c > " + gz_path_event + "/" + date_time +"/"  + date_time + "_" + str(i) + ".csv"
            list_cmd.append(cmd)
            # print(cmd)
    else:
        for hour in HOUR_DAY:
            cmd = "find " + gz_path_event + date_time + "/" + str(hour) + "/" + " -name \"*.gz\" " \
                                                                     "| xargs gunzip -c > " + gz_path_event+ "/" + date_time +"/" + date_time + "_" + str(
                hour) + ".csv"
            list_cmd.append(cmd)
            print(cmd)
    return list_cmd


def unzip_csv_install(date_time):
    s_file = "_SUCCESS"
    install_folders = glob.glob(gz_path_install + date_time + "/*/")
    downloaded_list = []
    list_cmd = []
    for folders in install_folders:
        if os.path.exists(folders + s_file):
            downloaded_list.append(folders)
    # print(downloaded_list)
    if len(downloaded_list) != len(HOUR_DAY):
        for i in range(len(downloaded_list)):
            cmd = "find " + downloaded_list[i] + " -name \"*.gz\" " \
                                                 "| xargs gunzip -c > " + gz_path_install + "/" + date_time + "/" + date_time + "_" + str(i) + ".csv"
            list_cmd.append(cmd)
            # print(cmd)
    else:
        for hour in HOUR_DAY:
            cmd = "find " + gz_path_install + date_time + "/" + str(hour) + "/" + " -name \"*.gz\" " \
                                                                     "| xargs gunzip -c > " + gz_path_install + "/" + date_time + "/" + date_time + "_" + str(
                hour) + ".csv"
            list_cmd.append(cmd)
            print(cmd)
    return list_cmd

def hour_add(df_in,time_columns_in,hours_in):
    df_out=df_in
    hour_add=timedelta(hours=hours_in)
    for column in time_columns_in:
        df_out[column]=df_out[column]+hour_add
    return df_out

def design_data(data_csv):
    df = pd.concat((pd.read_csv(f,
                    usecols=["event_name", "event_value", "appsflyer_id", "customer_user_id", "app_id", "event_time",
                              "install_time", "media_source", "app_version", "af_channel", "campaign", "ip",
                              "device_type", "os_version", "af_siteid", "idfa", "advertising_id"],
                    low_memory=False, dtype='unicode') for f in data_csv))
    df["customer_user_id"] = df["customer_user_id"].fillna('').astype(str)
    df["af_siteid"] = df["af_siteid"].fillna('').astype(str)
    df=df[df["install_time"]!="install_time"]
    df['install_time']=pd.to_datetime(df['install_time'],format= '%Y-%m-%d %H:%M:%S')
    df['event_time']=pd.to_datetime(df['event_time'],format= '%Y-%m-%d %H:%M:%S')
    df=hour_add(df,['install_time','event_time'],7)
    return df


def feather_processing(date_time):
    stime = time.time()
    all_files = glob.glob(gz_path_event + str(date_time) + "/*.csv")
    df = design_data(all_files)
    dest_directory = f"{parquet_path}event_"
    df.to_parquet(dest_directory + str(date_time) + ".parquet")
    print("Finished written " + parquet_path+"event_" + str(date_time) + ".parquet " + "in " + str(time.time() - stime) + " seconds")
    shutil.rmtree(gz_path_event)
    os.mkdir('data/gz/event')

    all_install_csv = glob.glob(gz_path_install + str(date_time) + "/*.csv")
    install_df = design_data(all_install_csv)
    install_df.to_parquet(f"{parquet_path}install_" + str(date_time) + ".parquet")
    print("Finished written " + parquet_path+"install_" + str(date_time) + ".parquet " + "in " + str(time.time() - stime) + " seconds")
    shutil.rmtree(gz_path_install)
    os.mkdir('data/gz/install')

def main():
    start = time.time()
    for DATE in LIST_DATE:
        date_time = datetime.strptime(DATE, '%Y-%m-%d').date()
        command_list = aws_sync(date_time)
        for command in command_list:
            command = command.split(" ")
            subprocess.run(command)

        csv_zip_cmd = unzip_csv(str(date_time))
        install_cmd = unzip_csv_install(str(date_time))

        for unzip_cmd in csv_zip_cmd:
            print(unzip_cmd)
            cmd = subprocess.Popen(unzip_cmd, stdin=subprocess.PIPE, shell=True)
            cmd.wait()

        for unzip_cmd in install_cmd:
            print(unzip_cmd)
            cmd = subprocess.Popen(unzip_cmd, stdin=subprocess.PIPE, shell=True)
            cmd.wait()

        # tao data partner file
        feather_processing(date_time)
    print("Run time: " + str(time.time() - start))

if __name__ == "__main__":
    main()
    